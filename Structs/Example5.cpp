#include <iostream>
struct Employee
{
	short id;
	int age;
	double wage;
};

int main()
{
	std::cout << "The size of Employee is " << sizeof(Employee) << "\n";

	//returns the offset of the field member from the start of the structure type
	std::cout << "offset of short id = " << offsetof(struct Employee, id) << std::endl;
	std::cout << "offset of int age = " << offsetof(struct Employee, age) << std::endl;
	std::cout << "offset of double wage = " << offsetof(struct Employee, wage) << std::endl;
	std::cout << "sizeof(Employee) = " << sizeof(Employee) << std::endl;
	
	return 0;
}