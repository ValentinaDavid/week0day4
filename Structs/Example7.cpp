#include <iostream>
struct  int_pointers 
{ 
	int  *ptr1, *ptr2; 
};


int main()
{
	int_pointers  ptrs;
	int  i1 = 154, i2;

	ptrs.ptr1 = &i1;
	ptrs.ptr2 = &i2;
	*ptrs.ptr2 = -97;
	std::cout << i1 << " " << *ptrs.ptr1 << std::endl;
	std::cout << i2 << " " << *ptrs.ptr2 << std::endl;
	return 0;
}